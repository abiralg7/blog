<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        ////rest the user table
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('users')->truncate(); 
        //generate 3 users
        DB::table('users')->insert([
            [
                'name' => "Abiral Gurung",
                'email' => "abiralg7@gmail.com",
                'password' => encrypt('password')
            ],
            [
                'name' => "Hari Gurung",
                'email' => "hari@gmail.com",
                'password' => encrypt('password')
            ],
            [
                'name' => "Ram Gurung",
                'email' => "ram@gmail.com",
                'password' => encrypt('password')
            ],
        ]);
    }
}
